package com.medicanimal.service;

/**
 * Created by Antonio Coviello on 23/05/16.
 */
public interface SettingsService {

    public void setProperty(String field, String value);

    public String getProperty(String prop);

    public String getDbUsername();
    public void setDbUsername(String dbUsername);

    public String getDbPassword();
    public void setDbPassword(String dbPassword);

    public String getDbName();
    public void setDbName(String dbName);

    public String getPaperTrailAuth();
    public void setPaperTrailAuth(String auth);
}
