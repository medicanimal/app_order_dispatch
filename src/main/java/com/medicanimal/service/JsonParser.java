package com.medicanimal.service;

import com.medicanimal.model.Order;
import com.medicanimal.model.PaperTrailGroup;

import java.util.List;

/**
 * Created by antonio on 17/05/16.
 */
public interface JsonParser {

    public List<Order> createOrderFromString(String json);

    public List<PaperTrailGroup> createGroupsFromString(String s);

    public String convertOrderToJson(Order ord);
}
