package com.medicanimal.service;

import com.medicanimal.model.Order;

/**
 * Created by Antonio Coviello on 19/05/16.
 */
public interface ServiceOrder {

    public void setBrokerUrl(String url);

    public String getBrokerUrl();

    public void processOrder(Order ord);

    public void completeOrder(Order ord);

    public void setDestination(String destination);

    public String getDestination();
}
