package com.medicanimal.service;


import  com.medicanimal.model.Order;

import java.util.List;

/**
 * Created by Antonio Coviello on 23/05/16.
 */
public interface LocalService {

    public boolean testConnection();

    public List<Order> findOrder(String orderId);
}
