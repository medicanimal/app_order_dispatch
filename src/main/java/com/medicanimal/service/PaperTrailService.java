package com.medicanimal.service;

import com.medicanimal.model.Order;
import com.medicanimal.model.PaperTrailGroup;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * Created by Antonio on 16/05/16.
 */
public interface PaperTrailService {

    public void setKey(String key);

    public List<Order> findOrder(String groupID, String orderId);

    public List<PaperTrailGroup> fetchAllGroup() throws IOException, URISyntaxException;
}
