package com.medicanimal.service.impl;

import com.medicanimal.model.Order;
import com.medicanimal.model.PaperTrailGroup;
import com.medicanimal.service.JsonParser;
import com.medicanimal.service.PaperTrailService;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by antonio on 17/05/16.
 */
public class PaperTrailServiceImpl implements PaperTrailService {

    private String key;
    private String group;
    private final String scheme = "https";
    private final String host = "papertrailapp.com";
    private final String path = "/api/v1";
    private final String queryPathSearch = "/events/search.json";
    private final String queryPathGroup = "/groups.json";
    private JsonParser jsonParser;

    public PaperTrailServiceImpl(JsonParser jsonParser){
        this.key = null;
        this.jsonParser = jsonParser;
    }

    public void setKey(String key) {
        this.key = key;
    }

    private Response request(String queryPath,Map<String,String> parameters) throws IOException, URISyntaxException {
        URIBuilder uri = new URIBuilder()
                .setScheme(scheme)
                .setHost(host)
                .setPath(path+queryPath);
        Iterator<String> iterator = parameters.keySet().iterator();
        String paramName,paramValue;
        while(iterator.hasNext()){
            paramName = iterator.next();
            paramValue = parameters.get(paramName);
            uri.setParameter(paramName,paramValue);
        }
        Response resp = Request.Get(uri.build())
                .addHeader("X-Papertrail-Token", key)
                .execute();
        if (resp==null){
            throw new RuntimeException("Error on request!");
        }
        return resp;
    }

    private boolean checkKey(){
        if (this.key != null){
            return true;
        }
        return false;
    }

    public List<Order> findOrder(String groupId,String orderId){
        if (groupId == null || groupId.trim().length()==0 ){
            throw new RuntimeException("groupId is null or empty");
        }
        if (orderId == null || orderId.trim().length()==0){
            throw new RuntimeException("OrderId is null or empty");
        }
        List<Order> orders = null;
        Map<String,String> mapParam = new HashMap<String, String>();
        mapParam.put("group_id",groupId);
        mapParam.put("q","orderId="+orderId);
        try {
            orders = jsonParser.createOrderFromString(request(queryPathSearch,mapParam).returnContent().asString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return orders;
    }

    public List<PaperTrailGroup> fetchAllGroup() throws IOException, URISyntaxException {
        List<PaperTrailGroup> papGroups= null;
        papGroups = jsonParser.createGroupsFromString(request(queryPathGroup, new HashMap<String, String>()).returnContent().asString());
        return papGroups;
    }

    public static void main(String[] args){
        PaperTrailService serv = new PaperTrailServiceImpl(new JsonParserGson());
        serv.setKey("c2pEVnSL0rRbjHLTxUDI");
        serv.findOrder("1322004","MA03758041");

    }


}


