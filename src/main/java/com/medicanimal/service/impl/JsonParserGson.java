package com.medicanimal.service.impl;

import com.google.gson.*;
import com.medicanimal.model.*;
import com.medicanimal.service.JsonParser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by antonio on 17/05/16.
 */
public class JsonParserGson implements JsonParser {

    private List<DispatchInfoItem> extractDispatchInfoItem(String msg){
        List<DispatchInfoItem> list = new ArrayList<DispatchInfoItem>();
        Pattern pattern = Pattern.compile("dispatchInfoItems.*?]");
        Matcher matcher = pattern.matcher(msg);
        String dispatchId;
        String orderEntryId;
        String productId;
        Integer quantityShipped;
        DispatchInfoItem di;
        while(matcher.find()){
            dispatchId = tryToExtract("dispatchId",matcher.group());
            orderEntryId = tryToExtract("orderEntryId",matcher.group());
            productId = tryToExtract("productId",matcher.group());
            try{
                quantityShipped = Integer.parseInt(tryToExtract("quantityShipped",matcher.group()));
            }catch (NumberFormatException ex){
                quantityShipped = null;
            }
            di = new DispatchInfoItem();
            di.setDispatchId(dispatchId);
            di.setOrderEntryId(orderEntryId);
            di.setProductId(productId);
            di.setQuantityShipped(quantityShipped);
            list.add(di);
        }
        return list;
    }

    private List<ConsignmentEntry> extractConsignmentEntry(String msg){
        ConsignmentEntry ce = null;
        Pattern pattern = Pattern.compile("consignmentEntries[ =]\\[.*?\\]");
        Matcher matcher = pattern.matcher(msg);
        String orderEntryId;
        String consignmentEntryId;
        String status;
        Integer orderQuantity;
        Integer shipQuantity;
        Integer allocatedQuantity;
        String productName;
        String productId;
        String stockExpectedDate;
        Boolean isOutOfStock;
        Integer consignmentEntryPK;
        List<ConsignmentEntry> ceList = new ArrayList<ConsignmentEntry>();
        while(matcher.find()){
            orderEntryId = tryToExtract("orderEntryId", matcher.group());
            consignmentEntryId = tryToExtract("consignmentEntryId", matcher.group());
            status=null;
            try {
                orderQuantity = Integer.parseInt(tryToExtract("orderQuantity", matcher.group()));
            }catch (NumberFormatException ex){ orderQuantity = null;}
            try{
                shipQuantity = Integer.parseInt(tryToExtract("shipQuantity", matcher.group()));
            }catch (NumberFormatException ex){ shipQuantity = null;}
            try{
                allocatedQuantity = Integer.parseInt(tryToExtract("allocatedQuantity", matcher.group()));
            }catch (NumberFormatException ex){allocatedQuantity = null;}
            try{
                consignmentEntryPK = Integer.parseInt(tryToExtract("consignmentEntryPK", matcher.group()));
            }catch (NumberFormatException ex){consignmentEntryPK = null;}
            productName = tryToExtract("productName",matcher.group());
            productId = tryToExtract("productId",matcher.group());
            stockExpectedDate=null;
            isOutOfStock = Boolean.parseBoolean(tryToExtract("isOutOfStock",matcher.group()));
            ce = new ConsignmentEntry();
            ce.setOrderEntryId(orderEntryId);
            ce.setConsignmentEntryId(consignmentEntryId);
            ce.setStatus(status);
            ce.setOrderQuantity(orderQuantity);
            ce.setShipQuantity(shipQuantity);
            ce.setAllocatedQuantity(allocatedQuantity);
            ce.setProductName(productName);
            ce.setProductId(productId);
            ce.setStockExpectedDate(stockExpectedDate);
            ce.setOutOfStock(isOutOfStock);
            ce.setConsignmentEntryPK(consignmentEntryPK);
            ceList.add(ce);
        }
        return ceList;
    }

    private List<DispatchInfoList> tryToExtractDispatchInfoList(String msg){
        List<DispatchInfoList> dispatchInfoList = new ArrayList<DispatchInfoList>();
        Pattern pattern = Pattern.compile("dispatchInfoList=\\[.*?\\]");
        Matcher matcher = pattern.matcher(msg);
        DispatchInfoList dil;
        String id;
        String reference;
        String warehouse;
        String courier;
        String trackingNumber;
        String dispatchDate;
        List<DispatchInfoItem> dispatchInfoItems;
        while(matcher.find()){
            String g = matcher.group();
            dispatchInfoItems = extractDispatchInfoItem(g);
            g = g.replaceAll("dispatchInfoItems.*?]","");
            id = tryToExtract("id",g);
            reference = tryToExtract("reference",g);
            warehouse = tryToExtract("warehouse",g);
            courier = tryToExtract("courier",g);
            trackingNumber = tryToExtract("trackingNumber",g);
            dispatchDate = tryToExtract("dispatchDate",g);
            dil = new DispatchInfoList();
            dil.setId(id);
            dil.setReference(reference);
            dil.setWarehouse(warehouse);
            dil.setCourier(courier);
            dil.setTrackingNumber(trackingNumber);
            dil.setDispatchDate(dispatchDate);
            dil.setDispatchInfoItems(dispatchInfoItems);
            dispatchInfoList.add(dil);
        }
        return dispatchInfoList;
    }

    private List<Consignment> tryToExtractConsignment(String msg){
        List<Consignment> consignments= new ArrayList<Consignment>();
        Pattern pattern = Pattern.compile("[, \\[]Consignment \\[.*?\\]\\].*?\\]");
        Matcher matcher = pattern.matcher(msg);
        String consignmentId;
        String orderId;
        String storeId;
        String siteId;
        String expectedStockArivalDate;
        Boolean exportedToWarehouse;
        String warehouseExportedTime;
        String warehouseId;
        String command;
        String status;
        List<ConsignmentEntry> consignmentEntries;
        String courier;
        String trackingNumber;
        String dispatchDate;
        Integer consignmentProcessId;
        String orderProcessId;
        Integer consignmentPK;
        Consignment consignment;
        while(matcher.find()){
            String g = matcher.group();
            consignmentEntries=extractConsignmentEntry(g);
            g = g.replaceAll(",[ ]consignmentEntries[ =]\\[.*?\\]","");
            consignmentId = tryToExtract("consignmentId",g);
            orderId = tryToExtract("orderId",g);
            storeId = tryToExtract("storeId",g);
            siteId = tryToExtract("siteId",g);
            expectedStockArivalDate = tryToExtract("expectedStockArivalDate",g);
            exportedToWarehouse = Boolean.parseBoolean(tryToExtract("exportedToWarehouse",g));
            warehouseExportedTime = tryToExtract("warehouseExportedTime",g);
            warehouseId = tryToExtract("warehouseId",g);
            command = tryToExtract("command",g);
            status = tryToExtract("status",g);
            courier = tryToExtract("courier",g);
            trackingNumber = tryToExtract("trackingNumber",g);
            dispatchDate = tryToExtract("dispatchDate",g);
            consignmentProcessId = Integer.parseInt(tryToExtract("consignmentProcessId",g));
            orderProcessId = tryToExtract("orderProcessId",g);
            consignmentPK = Integer.parseInt(tryToExtract("consignmentPK",g));
            consignment = new Consignment();
            consignment.setConsignmentEntries(consignmentEntries);
            consignment.setConsignmentId(consignmentId);
            consignment.setOrderId(orderId);
            consignment.setStoreId(storeId);
            consignment.setSiteId(siteId);
            consignment.setExpectedStockArivalDate(expectedStockArivalDate);
            consignment.setExportedToWarehouse(exportedToWarehouse);
            consignment.setWarehouseExportedTime(warehouseExportedTime);
            consignment.setWarehouseId(warehouseId);
            consignment.setCommand(command);
            consignment.setStatus(status);
            consignment.setCourier(courier);
            consignment.setTrackingNumber(trackingNumber);
            consignment.setDispatchDate(dispatchDate);
            consignment.setConsignmentProcessId(consignmentProcessId);
            consignment.setOrderProcessId(orderProcessId);
            consignment.setConsignmentPK(consignmentPK);
            consignments.add(consignment);

        }
        return consignments;
    }

    private List<String> extractMessageFromLog(String json){
        ArrayList<String> list = new ArrayList<String>();
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonObject jsonObj = gson.fromJson(json,JsonObject.class);
        JsonArray jsonArray = gson.fromJson(jsonObj.get("events"),JsonArray.class);
        for(int i=0;i<jsonArray.size();i++){
            JsonObject item = jsonArray.get(i).getAsJsonObject();
            list.add(item.get("message").getAsString());
        }
        return list;
    }

    private String tryToExtract(String property,String msg){
        //Pattern pattern = Pattern.compile(property+"=.*?[,\\]]");
        if(msg.charAt(msg.length()-1) != ']' || msg.charAt(msg.length()-1) != ',')
            msg+="]";
        Pattern pattern = Pattern.compile(property+"=.*?[,\\]]");
        Matcher m = pattern.matcher(msg);
        Set<String> str = new HashSet<String>();
        String t;
        while(m.find()){
            StringBuffer sb = new StringBuffer(m.group(0));
            t = sb.substring(property.length()+1,sb.length()-1);
            if(t.equalsIgnoreCase("null"))
                continue;
            str.add(t);
        }
        if(str.size()>1){
            str.remove("null");
            str.remove("\"null\"");
            if(str.size()>1){
                throw new RuntimeException("Try to search "+property+" but the property found are "+str.size());
            }
        }
        return (str.size()>=1)?str.iterator().next():null;
    }

    public List<Order> createOrderFromString(String json) {
        List<String> messages=extractMessageFromLog(json);
        List<Order> orders = new ArrayList<Order>();
        //consignment
        String orderId;
        String storeId;
        String siteId;
        String command;
        String parentProcessId;
        Order ord;
        List<Consignment> consignments;
        List<DispatchInfoList> dispatchInfoList;
        for(String item:messages){
            //consignments
            consignments = tryToExtractConsignment(item);
            //dispatchextract
            dispatchInfoList = tryToExtractDispatchInfoList(item);
            orderId=tryToExtract("orderId",item);
            storeId=tryToExtract("storeId",item);
            siteId=tryToExtract("siteId",item);
            command=tryToExtract("command",item);
            parentProcessId=tryToExtract("parentProcessId",item);
            ord = new Order();
            ord.setConsignments(consignments);
            ord.setDispatchInfoList(dispatchInfoList);
            ord.setOrderId(orderId);
            ord.setStoreId(storeId);
            ord.setSiteId(siteId);
            ord.setCommand(command);
            ord.setParentProcessId(parentProcessId);
            orders.add(ord);
        }
        return orders;
    }

    public List<PaperTrailGroup> createGroupsFromString(String json) {
        List<PaperTrailGroup> papGroups = new ArrayList<PaperTrailGroup>();
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonArray jsonArray = gson.fromJson(json,JsonArray.class);
        String id,name;
        JsonObject obj;
        PaperTrailGroup group;
        for(JsonElement item:jsonArray){
            obj = gson.fromJson(item,JsonObject.class);
            id = obj.get("id").getAsString();
            name = obj.get("name").getAsString();
            group = new PaperTrailGroup();
            group.setGroupId(id);
            group.setName(name);
            papGroups.add(group);
        }
        return papGroups;
    }

    public String convertOrderToJson(Order ord) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(ord);
    }

    public static void main(String[] args){
        String test = "{\"min_id\":\"669216521213730827\",\"max_id\":\"669216521213730827\",\"events\":[{\"id\":\"669216521213730827\",\"source_ip\":\"54.171.35.207\",\"program\":\"console-20160517.log\",\"message\":\"INFO   | jvm 1    | main    | 2016/05/17 13:14:33.542 | \\u001B[0;32mINFO  [org.medicanimal.fulfilment.jms.listener.NonStartingUpMessageListenerContainer#0-1] [AbstractWarehouseConsignmentMessageConsumer] Received message:  OrderConsignment [orderId=MA03748028, storeId=4, siteId=ma-GB, command=prepare, consignments=[Consignment [consignmentId=MA03748028, orderId=MA03748028, storeId=4, siteId=null, expectedStockArivalDate=null, exportedToWarehouse=false, warehouseExportedTime=null, warehouseId=MYWAREHOUSE01, command=prepare, status=Created, consignmentEntries=[ConsignmentEntry [orderId=null, orderEntryId=1, consignmentEntryId=1, status=null, orderQuantity=1, shipQuantity=0, allocatedQuantity=0, productName=Pharma Zeal (Boma Zeal) Senior Dog Tabs 100 Tablets, productId=I0000033, stockExpectedDate=null, isOutOfStock=false, consignmentEntryPK=0]], courier=, trackingNumber=, dispatchDate=null, consignmentProcessId=0, orderProcessId=null, consignmentPK=0], Consignment [consignmentId=MA03748028-1, orderId=MA03748028, storeId=4, siteId=null, expectedStockArivalDate=null, exportedToWarehouse=false, warehouseExportedTime=null, warehouseId=MYPRESCRIPTIONS01, command=prepare, status=Created, consignmentEntries=[ConsignmentEntry [orderId=null, orderEntryId=0, consignmentEntryId=0, status=null, orderQuantity=1, shipQuantity=0, allocatedQuantity=0, productName=Panacur Equine Wormer Paste 24g, productId=I0000125, stockExpectedDate=null, isOutOfStock=false, consignmentEntryPK=0]], courier=, trackingNumber=, dispatchDate=null, consignmentProcessId=0, orderProcessId=null, consignmentPK=0]], dispatchInfoList=[], parentProcessId=9197994409982]\",\"received_at\":\"2016-05-17T06:14:34-07:00\",\"generated_at\":\"2016-05-17T06:14:34-07:00\",\"display_received_at\":\"May 17 06:14:34\",\"source_id\":110893793,\"source_name\":\"aws-hybris5systest-respecom-app22\",\"hostname\":\"aws-hybris5systest-respecom-app22\",\"severity\":\"Notice\",\"facility\":\"User\"}],\"reached_time_limit\":true,\"min_time_at\":\"2016-05-17T05:56:35-07:00\"}";
        String test1 = "{\"min_id\":\"669524152235089935\",\"max_id\":\"669530244801388546\",\"events\":[{\"id\":\"669524152235089935\",\"source_ip\":\"54.171.91.120\",\"program\":\"console-20160518.log\",\"message\":\"INFO   | jvm 1    | main    | 2016/05/18 09:36:58.504 | \\u001B[0;32mINFO  [org.medicanimal.fulfilment.jms.listener.NonStartingUpMessageListenerContainer#0-1] [AbstractWarehouseConsignmentMessageConsumer] Received message:  OrderConsignment [orderId=MA03758041, storeId=4, siteId=ma-GB, command=prepare, consignments=[Consignment [consignmentId=MA03758041, orderId=MA03758041, storeId=4, siteId=null, expectedStockArivalDate=null, exportedToWarehouse=false, warehouseExportedTime=null, warehouseId=MYWAREHOUSE01, command=prepare, status=Created, consignmentEntries=[ConsignmentEntry [orderId=null, orderEntryId=0, consignmentEntryId=0, status=null, orderQuantity=1, shipQuantity=0, allocatedQuantity=0, productName=Hill's Prescription Diet Canine i/d 12kg, productId=I0007169, stockExpectedDate=null, isOutOfStock=false, consignmentEntryPK=0]], courier=, trackingNumber=, dispatchDate=null, consignmentProcessId=0, orderProcessId=null, consignmentPK=0]], dispatchInfoList=[], parentProcessId=9198847361022]\",\"received_at\":\"2016-05-18T02:36:59-07:00\",\"generated_at\":\"2016-05-18T02:36:59-07:00\",\"display_received_at\":\"May 18 02:36:59\",\"source_id\":110893903,\"source_name\":\"aws-hybris5systest-respecom-app21\",\"hostname\":\"aws-hybris5systest-respecom-app21\",\"severity\":\"Notice\",\"facility\":\"User\"},{\"id\":\"669529148242550788\",\"source_ip\":\"54.171.91.120\",\"program\":\"console-20160518.log\",\"message\":\"INFO   | jvm 1    | main    | 2016/05/18 09:56:49.136 | \\u001B[0;32mINFO  [org.medicanimal.fulfilment.jms.listener.NonStartingUpMessageListenerContainer#0-2] [AbstractWarehouseConsignmentMessageConsumer] Received message:  OrderConsignment [orderId=MA03758041, storeId=4, siteId=ma-GB, command=processing, consignments=[Consignment [consignmentId=MA03758041, orderId=MA03758041, storeId=4, siteId=null, expectedStockArivalDate=18/05/2016 09:18:00, exportedToWarehouse=true, warehouseExportedTime=18/05/2016 09:22:29, warehouseId=MYWAREHOUSE01, command=processing, status=Created, consignmentEntries=[ConsignmentEntry [orderId=null, orderEntryId=0, consignmentEntryId=0, status=null, orderQuantity=1, shipQuantity=0, allocatedQuantity=1, productName=Hill's Prescription Diet Canine i/d 12kg, productId=I0007169, stockExpectedDate=null, isOutOfStock=false, consignmentEntryPK=0]], courier=, trackingNumber=, dispatchDate=null, consignmentProcessId=0, orderProcessId=null, consignmentPK=0]], dispatchInfoList=[], parentProcessId=9198847361022]\",\"received_at\":\"2016-05-18T02:56:50-07:00\",\"generated_at\":\"2016-05-18T02:56:50-07:00\",\"display_received_at\":\"May 18 02:56:50\",\"source_id\":110893903,\"source_name\":\"aws-hybris5systest-respecom-app21\",\"hostname\":\"aws-hybris5systest-respecom-app21\",\"severity\":\"Notice\",\"facility\":\"User\"},{\"id\":\"669530244801388546\",\"source_ip\":\"54.171.58.82\",\"program\":\"console-20160518.log\",\"message\":\"INFO   | jvm 1    | main    | 2016/05/18 10:01:10.553 | \\u001B[0;32mINFO  [org.medicanimal.fulfilment.jms.listener.NonStartingUpMessageListenerContainer#0-1] [AbstractWarehouseConsignmentMessageConsumer] Received message:  OrderConsignment [orderId=MA03758041, storeId=4, siteId=ma-GB, command=delivered, consignments=[Consignment [consignmentId=MA03758041, orderId=MA03758041, storeId=4, siteId=null, expectedStockArivalDate=18/05/2016 09:18:00, exportedToWarehouse=true, warehouseExportedTime=18/05/2016 09:22:29, warehouseId=MYWAREHOUSE01, command=delivered, status=Shipped, consignmentEntries=[ConsignmentEntry [orderId=null, orderEntryId=0, consignmentEntryId=0, status=null, orderQuantity=1, shipQuantity=1, allocatedQuantity=1, productName=Hill's Prescription Diet Canine i/d 12kg, productId=I0007169, stockExpectedDate=null, isOutOfStock=false, consignmentEntryPK=0]], courier=, trackingNumber=, dispatchDate=null, consignmentProcessId=0, orderProcessId=null, consignmentPK=0]], dispatchInfoList=[DispatchInfo [id=1234, reference=MA03758041::MA03758041, warehouse=MYWAREHOUSE01, courier=DPD, trackingNumber=ABCDES111, dispatchDate=18/05/2016 09:32:08, dispatchInfoItems=[DispatchInfoItem [dispatchId=1234, orderEntryId=0, productId=I0015241, qunatityShipped=1]]]], parentProcessId=9198847361022]\",\"received_at\":\"2016-05-18T03:01:12-07:00\",\"generated_at\":\"2016-05-18T03:01:11-07:00\",\"display_received_at\":\"May 18 03:01:12\",\"source_id\":78071154,\"source_name\":\"aws-hybris5systest-ecom-app02\",\"hostname\":\"aws-hybris5systest-ecom-app02\",\"severity\":\"Notice\",\"facility\":\"User\"}],\"reached_time_limit\":true,\"min_time_at\":\"2016-05-18T01:58:18-07:00\"}";
        List<Order> ord = new JsonParserGson().createOrderFromString(test1);
        System.out.println("shit");
    }
}
