package com.medicanimal.service.impl;

import com.medicanimal.service.SettingsService;
import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.SystemConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.*;
import java.net.URL;
import java.util.Properties;

/**
 * Created by Antonio Coviello on 23/05/16.
 */
public class SettingsServiceIOImpl implements SettingsService{

    private final static String DBNAME = "DBNAME";
    private final static String DBUSER = "DBUSER";
    private final static String DBPASS = "DBPASS";
    private final static String PAPER_AUTH_KEY = "PAPERTRAIL_AUTH_KEY";
    private final static String FILE_PATH = "dispatch_order.conf";

    private Configurations configuration;

    public SettingsServiceIOImpl() {
        File f = new File(FILE_PATH);
        if(!f.exists())
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    public String getProperty(String prop){
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(FILE_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty(prop);
    }

    public void setProperty(String field, String value) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(FILE_PATH));
            properties.setProperty(field,value);
            properties.store(new FileOutputStream(FILE_PATH),null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getDbUsername() {
        return getProperty(DBUSER);
    }

    public void setDbUsername(String dbUsername) {
        setProperty(DBUSER,dbUsername);
    }

    public String getDbPassword() {
        return getProperty(DBPASS);
    }

    public void setDbPassword(String dbPassword) {
        setProperty(DBPASS,dbPassword);
    }

    public String getDbName() {
        return getProperty(DBNAME);
        /*return "jdbc:mysql://localhost/madev";*/
    }

    public void setDbName(String dbName) {
        setProperty(DBNAME,dbName);
    }

    public String getPaperTrailAuth() {
        return getProperty(PAPER_AUTH_KEY);
    }

    public void setPaperTrailAuth(String auth) {
        setProperty(PAPER_AUTH_KEY,auth);
    }

}
