package com.medicanimal.service.impl;

import com.medicanimal.model.Order;
import com.medicanimal.service.DbAdapter;
import com.medicanimal.service.LocalService;

import java.util.List;

/**
 * Created by Antonio Coviello on 23/05/16.
 */
public class LocalServiceImpl implements LocalService{

    private DbAdapter dbAdapter;

    public LocalServiceImpl(){
        dbAdapter = new MysqlDbAdapter();
    }

    public boolean testConnection() {
        return dbAdapter.testDb();
    }

    public List<Order> findOrder(String orderId) {
        return dbAdapter.fetchOrder(orderId);
    }
}
