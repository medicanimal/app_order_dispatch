package com.medicanimal.service.impl;

import com.medicanimal.model.*;
import com.medicanimal.service.JsonParser;
import com.medicanimal.service.ServiceOrder;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Antonio Coviello on 19/05/16.
 */
public class ServiceOrderImpl implements ServiceOrder {

    private JsonParser jsonParser;
    private String brokerUrl;
    private String destination;

    public ServiceOrderImpl(JsonParser p){
        jsonParser = p;
    }

    public String getBrokerUrl(){
        if(this.brokerUrl == null){
            throw  new RuntimeException("No brokerurl setted");
        }
        return this.brokerUrl;
    }

    public void setBrokerUrl(String url){
        this.brokerUrl = url;
    }

    private JsonParser getJsonParserService(){
        if(jsonParser !=null)
            return jsonParser;
        else
            throw new RuntimeException("No service jsonParser in ServiceOrderImpl");
    }

    public void processOrder(Order ord) {
        //set order in process mode
        ord.setCommand("processing");
        //calculate date
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -2);
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date());
        cal1.add(Calendar.HOUR, -2);
        cal1.add(Calendar.MINUTE, 5);
        for(Consignment c: ord.getConsignments()){
            c.setExpectedStockArivalDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(cal.getTime()));
            c.setWarehouseExportedTime(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(cal1.getTime()));
            c.setCommand("processing");
            for(ConsignmentEntry ce: c.getConsignmentEntries()) {
                ce.setShipQuantity(0);
                ce.setOrderQuantity(1);
            }
            c.setExportedToWarehouse(true);
        }
        ord.setDispatchInfoList(new ArrayList<DispatchInfoList>());
        String strJson =  getJsonParserService().convertOrderToJson(ord);
        sendMessageJMS(strJson);
        System.out.println(strJson);
    }

    /* TO DO: improve it to dispatch all product in the order. For now it work only with one product,
    * but on the papertrail system it alredy should work */
    public void completeOrder(Order ord) {
        ord.setCommand("delivered");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -2);
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(new Date());
        cal1.add(Calendar.HOUR, -2);
        cal1.add(Calendar.MINUTE, 5);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(new Date());
        cal2.add(Calendar.HOUR, -2);
        cal2.add(Calendar.MINUTE, 20);
        String warehouse=null;
        String productId = null;
        for(Consignment c: ord.getConsignments()){
            c.setExpectedStockArivalDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(cal.getTime()));
            c.setWarehouseExportedTime(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(cal1.getTime()));
            c.setCommand("delivered");
            c.setStatus("Shipped");
            for(ConsignmentEntry ce: c.getConsignmentEntries()) {
                ce.setShipQuantity(1);
                ce.setOrderQuantity(1);
                ce.setAllocatedQuantity(1);
                productId = ce.getProductId();
            }
            c.setExportedToWarehouse(true);
            warehouse = c.getWarehouseId();
        }
        for(DispatchInfoList dil: ord.getDispatchInfoList()){
            dil.setId("1234");
            dil.setCourier("DPD");
            dil.setWarehouse(warehouse);
            dil.setReference(ord.getOrderId()+"::"+ord.getOrderId());
            dil.setTrackingNumber("ABCDES111");
            dil.setDispatchDate(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(cal2.getTime()));
            for(DispatchInfoItem item:dil.getDispatchInfoItems()){
                item.setDispatchId("1234");
                item.setOrderEntryId("0");
                item.setProductId(productId);
                item.setQuantityShipped(1);
            }
        }
        String strJson =  getJsonParserService().convertOrderToJson(ord);
        sendMessageJMS(strJson);
        System.out.println(strJson);
    }

    private void sendMessageJMS(String strJson) {
        try{
            ConnectionFactory factory = new ActiveMQConnectionFactory(getBrokerUrl());
            Connection connection = factory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue(this.getDestination());
            MessageProducer producer =session.createProducer(destination);
            TextMessage msg = session.createTextMessage();
            msg.setText(strJson);
            /*msg.setText("{\n" +
                    "\t\"orderId\": \"MA00011000\",\n" +
                    "\t\"storeId\": \"4\",\n" +
                    "\t\"siteId\": \"ma-GB\",\n" +
                    "\t\"command\": \"processing\",\n" +
                    "\t\"consignments\": [{\n" +
                    "\t\t\"consignmentId\": \"MA00011000\",\n" +
                    "\t\t\"orderId\": \"MA00011000\",\n" +
                    "\t\t\"storeId\": \"4\",\n" +
                    "\t\t\"siteId\": null,\n" +
                    "\t\t\"expectedStockArivalDate\": \"23/05/2016 11:18:00\",\n" +
                    "\t\t\"exportedToWarehouse\": true,\n" +
                    "\t\t\"warehouseExportedTime\": \"23/05/2016 11:22:29\",\n" +
                    "\t\t\"warehouseId\": \"default\",\n" +
                    "\t\t\"command\": \"processing\",\n" +
                    "\t\t\"status\": \"Created\",\n" +
                    "\t\t\"consignmentEntries\": [{\n" +
                    "\t\t\t\"orderEntryId\": \"0\",\n" +
                    "\t\t\t\"consignmentEntryId\": \"0\",\n" +
                    "\t\t\t\"status\": null,\n" +
                    "\t\t\t\"orderQuantity\": 1,\n" +
                    "\t\t\t\"shipQuantity\": 0,\n" +
                    "\t\t\t\"allocatedQuantity\": 1,\n" +
                    "\t\t\t\"productName\": \"Happy Dog Supreme Adult Sensible Africa\",\n" +
                    "\t\t\t\"productId\": \"I0016198\",\n" +
                    "\t\t\t\"stockExpectedDate\": null,\n" +
                    "\t\t\t\"isOutOfStock\": false,\n" +
                    "\t\t\t\"consignmentEntryPK\":0\n" +
                    "\t\t}],\n" +
                    "\t\t\"courier\": \"\",\n" +
                    "\t\t\"trackingNumber\":\"\",\n" +
                    "\t\t\"dispatchDate\":null,\n" +
                    "\t\t\"consignmentProcessId\": 0,\n" +
                    "\t\t\"orderProcessId\": null,\n" +
                    "\t\t\"consignmentPK\": 0\n" +
                    "\t}],\n" +
                    "\t\"dispatchInfoList\":[],\n" +
                    "\t\"parentProcessId\": 8796486303742\n" +
                    "}\n");*/
            producer.send(msg);
            connection.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public String getDestination(){
        return this.destination;
    }

    public void setDestination(String d){
        this.destination = d;
    }

    public static void main(String[] args){
        Order ord = new Order();
        ord.setOrderId("MA00010001");
        ord.setStoreId("4");
        ord.setSiteId("ma-GB");
        ServiceOrderImpl i = new ServiceOrderImpl(new JsonParserGson());
        //i.setDestination("dev.incoming.warehouse.client.queue");
        i.setDestination("dev.consignment.export.boytrix.queue");
        i.setBrokerUrl("failover://tcp://localhost:61616");


        ord.setConsignments(new ArrayList<Consignment>());
        ord.setParentProcessId("8796486303742");
        i.processOrder(ord);
        System.exit(0);
    }

}
