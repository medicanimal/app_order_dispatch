package com.medicanimal.service.impl;

import com.medicanimal.model.Consignment;
import com.medicanimal.model.ConsignmentEntry;
import com.medicanimal.model.DispatchInfoList;
import com.medicanimal.model.Order;
import com.medicanimal.service.DbAdapter;
import com.medicanimal.service.SettingsService;
import org.apache.commons.dbcp.BasicDataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Antonio Coviello on 23/05/16.
 */
public class MysqlDbAdapter implements DbAdapter {

    private SettingsService settingsService;
    private BasicDataSource ds;

    public MysqlDbAdapter(){
        settingsService = new SettingsServiceIOImpl();
    }

    private Connection getConnection(){
        if(ds==null)
            throw new RuntimeException("Error no datasource initialized!");
        Connection conn = null;
        try {
            conn = ds.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public boolean testDb() {
        String username = settingsService.getDbUsername();
        String password = settingsService.getDbPassword();
        String dbName = settingsService.getDbName();
        ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(dbName);
        ds.setMinIdle(5);
        ds.setMaxIdle(20);
        ds.setMaxOpenPreparedStatements(180);
        try {
            getConnection().close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<Order> fetchOrder(String orderId) {
        Map<String,Order> map = new HashMap<String, Order>();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement("SELECT o.p_code as order_id,p.p_orderprocess as p_id,orde.p_info as p_info FROM orders o LEFT JOIN processes p on o.PK = p.p_order LEFT JOIN orderentries orde on o.PK = orde.p_order where p.p_orderprocess IS NOT NULL and o.p_code = ?;");
            ps.setString(1,orderId);
            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                Order ord = (map.get(orderId) != null)?map.get(orderId):new Order();
                ord.setOrderId(rs.getString("order_id"));
                ord.setStoreId("4");
                ord.setSiteId("ma-GB");
                List<Consignment> consignments = ord.getConsignments();
                Consignment consignment;
                if (consignments != null && consignments.size()>0) {
                    consignment = consignments.get(0);
                }else{
                    consignments = new ArrayList<Consignment>();
                    consignment = new Consignment();
                    consignment.setConsignmentId(orderId);
                    consignment.setOrderId(orderId);
                    consignment.setStoreId("4");
                    consignment.setWarehouseId("default");
                    consignment.setConsignmentProcessId(0);
                    consignment.setConsignmentPK(0);
                    consignments.add(consignment);
                }

                List<ConsignmentEntry> ceLists = (consignment.getConsignmentEntries()!=null)?consignment.getConsignmentEntries():new ArrayList<ConsignmentEntry>();
                ConsignmentEntry ce = new ConsignmentEntry();
                ce.setOrderEntryId("0");
                ce.setConsignmentEntryId("0");
                ce.setOrderQuantity(1);
                ce.setShipQuantity(0);
                ce.setAllocatedQuantity(1);
                ce.setProductId(rs.getString("p_info").substring(9,17));
                ce.setProductName("");
                ce.setOutOfStock(false);
                ceLists.add(ce);
                consignment.setConsignmentEntries(ceLists);
                ord.setDispatchInfoList(new ArrayList<DispatchInfoList>());
                ord.setConsignments(consignments);
                ord.setParentProcessId(rs.getString("p_id"));

                map.put(orderId,ord);
            }
            ps.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<Order>(map.values());
    }
}
