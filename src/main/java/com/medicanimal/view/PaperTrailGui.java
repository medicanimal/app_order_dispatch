package com.medicanimal.view;

import com.medicanimal.model.Order;
import com.medicanimal.model.PaperTrailGroup;
import com.medicanimal.service.JsonParser;
import com.medicanimal.service.PaperTrailService;
import com.medicanimal.service.ServiceOrder;
import com.medicanimal.service.SettingsService;
import com.medicanimal.service.impl.JsonParserGson;
import com.medicanimal.service.impl.PaperTrailServiceImpl;
import com.medicanimal.service.impl.ServiceOrderImpl;
import com.medicanimal.service.impl.SettingsServiceIOImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by antonio on 18/05/16.
 */
public class PaperTrailGui {

    private static final Image IMAGE = new Image("img/logo.gif");
    private PaperTrailService paperTrailService;
    private ServiceOrder serviceOrder;
    private JsonParser jsonParser;
    private final Label keyLabel;
    private final PasswordField keyPasswordField;
    private final Button logInButton;
    private final Label findGroup;
    private final ComboBox<PaperTrailGroup> comboBoxGroup;
    private final Label searchLabel;
    private final TextField searchTextField;
    private final Button searchButton;
    private final TableView resultOrder;
    private final Button processOrderButton;
    private final Button dispatchOrderButton;
    private final Button changeKeyButton;
    private final Button backButton;
    private final GridPane backGridPane;

    private final Stage stage;

    private final GridPane searchGridPane;
    private final GridPane loginGridPane;
    private final GridPane logoGridPane;
    private ObservableList<Order> ordersList;
    private SettingsService settings;


    public PaperTrailGui(Stage st){
        stage = st;
        serviceOrder = null;
        jsonParser = new JsonParserGson();
        paperTrailService = new PaperTrailServiceImpl(jsonParser);
        keyLabel = new Label("Put your authorization key:");
        keyPasswordField = new PasswordField();
        logInButton = new Button("Log in");
        findGroup = new Label("Choose the system:");
        comboBoxGroup = new ComboBox<PaperTrailGroup>();
        searchLabel = new Label("Search for your order:");
        searchTextField = new TextField();
        searchButton = new Button("Search");
        searchGridPane = new GridPane();
        loginGridPane = new GridPane();
        logoGridPane = new GridPane();
        resultOrder = new TableView();
        ordersList = FXCollections.observableArrayList();
        processOrderButton = new Button("Process order");
        dispatchOrderButton = new Button("Complete order");
        changeKeyButton = new Button("Change auth key");
        backGridPane = new GridPane();
        backButton = new Button("Back");
        settings = new SettingsServiceIOImpl();
        st.setScene(buildScene());
    }

    public JsonParser getJsonParserService(){
        if (jsonParser == null){
            jsonParser = new JsonParserGson();
        }
        return jsonParser;
    }

    public PaperTrailService getPaperTrailService(){
        if(paperTrailService == null){
            paperTrailService = new PaperTrailServiceImpl(getJsonParserService());
        }
        return paperTrailService;
    }

    public ServiceOrder getServiceOrder(){
        if (this.serviceOrder == null){
            serviceOrder = new ServiceOrderImpl(getJsonParserService());
        }
        return serviceOrder;
    }

    public Scene buildScene(){



        final ImageView imageView = new ImageView(IMAGE);

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(15,15,0,15));

        logoGridPane.setAlignment(Pos.TOP_CENTER);
        logoGridPane.setHgap(10);
        logoGridPane.setVgap(10);
        logoGridPane.setPadding(new Insets(15,15,0,15));

        loginGridPane.setAlignment(Pos.TOP_CENTER);
        loginGridPane.setHgap(10);
        loginGridPane.setVgap(10);
        loginGridPane.setPadding(new Insets(15,15,0,15));

        searchGridPane.setAlignment(Pos.TOP_CENTER);
        searchGridPane.setHgap(10);
        searchGridPane.setVgap(10);
        searchGridPane.setPadding(new Insets(15,15,0,15));

        backGridPane.setAlignment(Pos.TOP_CENTER);
        backGridPane.setHgap(10);
        backGridPane.setVgap(10);
        backGridPane.setPadding(new Insets(15,15,0,15));

        Group imageGroup = new Group(imageView);
        logoGridPane.add(imageGroup,0,0,3,1);




        loginGridPane.add(keyLabel,0,1);
        keyPasswordField.setPrefWidth(190);
        loginGridPane.add(keyPasswordField,1,1);
        logInButton.setPrefWidth(70);
        loginGridPane.add(logInButton,2,1);





        //findGroup.setVisible(false);
        //grid.add(findGroup,0,2);
        searchGridPane.add(findGroup,0,0);


        //comboBoxGroup.setVisible(false);
        //grid.add(comboBoxGroup,1,2,2,1);
        searchGridPane.add(comboBoxGroup,1,0,2,1);


        //searchLabel.setVisible(false);
        //grid.add(searchLabel,0,3);
        searchGridPane.add(searchLabel,0,1);


        //searchTextField.setVisible(false);
        //grid.add(searchTextField,1,3);
        searchGridPane.add(searchTextField,1,1);

        //searchButton.setVisible(false);
        //grid.add(searchButton,2,3);
        searchGridPane.add(searchButton,2,1);



        resultOrder.setEditable(false);
        resultOrder.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        resultOrder.setPrefHeight(150);
        TableColumn orderIdColumn = new TableColumn("Id");
        orderIdColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("orderIdProperty"));
        TableColumn siteIdColumn = new TableColumn("Site");
        siteIdColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("siteIdProperty"));
        TableColumn statusColumn = new TableColumn("Status");
        statusColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("statusProperty"));
        TableColumn pIdColumn = new TableColumn("Parent process Id");
        pIdColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("pIdProperty"));
        resultOrder.setItems(ordersList);
        resultOrder.getColumns().addAll(orderIdColumn,siteIdColumn,statusColumn,pIdColumn);
        searchGridPane.add(resultOrder,0,2,3,1);


        searchGridPane.add(processOrderButton,0,3);
        searchGridPane.add(dispatchOrderButton,1,3);
        searchGridPane.add(changeKeyButton,2,3);

        backButton.setPrefWidth(150);
        backGridPane.add(backButton,0,3);

        grid.add(logoGridPane,0,0);
        grid.add(loginGridPane,0,1);
        grid.add(searchGridPane,0,2);
        grid.add(backGridPane,0,3);


        loginGridPane.setVisible(true);
        searchGridPane.setVisible(false);






        //Scene scene = new Scene(grid,560,630, Color.AZURE);
        Scene scene = new Scene(grid,560,580);
        scene.getStylesheets().add(PaperTrailGui.class.getResource("/css/style.css").toExternalForm());



        keyPasswordField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode().equals(KeyCode.ENTER)){
                    if(keyPasswordField.getText().trim().length()!=0)
                        logIn();
                    //else
                        //generate Exception
                }
            }
        });

        logInButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if(keyPasswordField.getText().trim().length()!=0)
                    logIn();
                //else
                    //generate Exception
            }
        });

        searchTextField.setOnKeyPressed(new EventHandler<KeyEvent>() {
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode().equals(KeyCode.ENTER)){
                    searchOrder();
                }else{
                    //generate Exception
                }
            }
        });

        searchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if(searchTextField.getText().trim().length()!=0)
                    searchOrder();
                //else
                    //generate exception
            }
        });

        changeKeyButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                searchGridPane.setVisible(false);
                loginGridPane.setDisable(false);
                keyPasswordField.setText("");
                settings.setPaperTrailAuth("");
            }
        });

        processOrderButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                Order ord = (Order)resultOrder.getSelectionModel().getSelectedItem();
                if(ord != null){
                    processOrder(ord);
                }
            }
        });

        dispatchOrderButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                Order ord = (Order)resultOrder.getSelectionModel().getSelectedItem();
                if(ord!=null)
                    completeOrder(ord);
            }
        });

        backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                GuiChoose gui = new GuiChoose(stage);
            }
        });

        keyPasswordField.setText(settings.getPaperTrailAuth());
        if(settings.getPaperTrailAuth()!=null && settings.getPaperTrailAuth().length()>0){
            logIn();
        }

        return scene;

    }

    private void completeOrder(Order ord){
        searchGridPane.setDisable(true);
        getServiceOrder().setDestination("hybris5systest.consignment.export.boytrix.queue");
        getServiceOrder().setBrokerUrl("tcp://aws-hybris5systest-ecom-activemq02.medicanimal.net:61616");
        getServiceOrder().completeOrder(ord);
        searchGridPane.setDisable(false);
    }

    private void processOrder(Order ord){
        searchGridPane.setDisable(true);
        getServiceOrder().setDestination("hybris5systest.consignment.export.boytrix.queue");
        getServiceOrder().setBrokerUrl("tcp://aws-hybris5systest-ecom-activemq02.medicanimal.net:61616");
        getServiceOrder().processOrder(ord);
        searchGridPane.setDisable(false);
    }

    private void logIn(){
        loginGridPane.setDisable(true);
        //keyPasswordField.setDisable(true);
        paperTrailService.setKey(keyPasswordField.getText());
        List<PaperTrailGroup> groups = new ArrayList<PaperTrailGroup>();
        try{
            groups = paperTrailService.fetchAllGroup();
        }catch (URISyntaxException ex){
            System.out.print(ex.getMessage());
        }catch(IOException ex){
            System.out.println(ex.getMessage());
        }
        if(groups.size()>0) {
            keyPasswordField.setStyle("-fx-control-inner-background: #FFFFFF");
            ObservableList<PaperTrailGroup> data = FXCollections.observableArrayList(groups);
            comboBoxGroup.setItems(data);
            findGroup.setVisible(true);
            int idx = 0;
            for (PaperTrailGroup group : groups) {
                if (group.getName().toLowerCase().contains("systest")) {
                    break;
                }
                idx++;
            }
            comboBoxGroup.setValue(groups.get(idx));
            comboBoxGroup.setPrefWidth(300);
            searchGridPane.setVisible(true);
            loginGridPane.setDisable(true);
            settings.setPaperTrailAuth(keyPasswordField.getText());
            /*comboBoxGroup.setVisible(true);
            searchLabel.setVisible(true);
            searchTextField.setVisible(true);
            keyPasswordField.setDisable(true);
            logInButton.setDisable(true);
            keyLabel.setDisable(true);*/
        }else{
            keyPasswordField.setStyle("-fx-control-inner-background: #ffa1a1");
            loginGridPane.setDisable(false);
            /*keyPasswordField.setDisable(false);
            logInButton.setDisable(false);
            keyLabel.setDisable(false);*/
        }
    }

    private void searchOrder(){
        String orderId = searchTextField.getText().trim();
        PaperTrailGroup groupId = comboBoxGroup.getValue();
        if(groupId != null &&
                groupId.getGroupId()!=null &&
                groupId.getGroupId().trim().length()!=0 &&
                orderId.length()!=0){
            searchGridPane.setDisable(true);
            List<Order> orders = paperTrailService.findOrder(groupId.getGroupId(),orderId);
            ordersList.removeAll(resultOrder.getItems());
            ordersList.addAll(orders);
            searchGridPane.setDisable(false);
        }else{
            return; //generate exception
        }
    }

}
