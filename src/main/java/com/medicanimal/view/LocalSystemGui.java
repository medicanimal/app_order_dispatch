package com.medicanimal.view;

import com.medicanimal.model.DispatchInfoItem;
import com.medicanimal.model.DispatchInfoList;
import com.medicanimal.model.Order;
import com.medicanimal.service.JsonParser;
import com.medicanimal.service.LocalService;
import com.medicanimal.service.ServiceOrder;
import com.medicanimal.service.SettingsService;
import com.medicanimal.service.impl.JsonParserGson;
import com.medicanimal.service.impl.LocalServiceImpl;
import com.medicanimal.service.impl.ServiceOrderImpl;
import com.medicanimal.service.impl.SettingsServiceIOImpl;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio Coviello on 23/05/16.
 */
public class LocalSystemGui {

    private static final Image IMAGE = new Image("img/logo.gif");

    private LocalService localService;

    private final Label dbNameLabel;
    private final TextField dbNameTxt;
    private final Label dbUserLabel;
    private final TextField dbUserTxt;
    private final Label dbPasswordLabel;
    private final PasswordField dbPasswordTxt;
    private final GridPane dbPane;
    private final Stage stage;
    private final GridPane gridPane;
    private final GridPane searchGridPane;
    private final Label searchCodeLabel;
    private final TextField searchCodeTxt;
    private final Button searchButton;
    private final TableView resultOrder;
    private ObservableList<Order> ordersList;
    private SettingsService settings;
    private final Button connectDbButton;
    private final TitledPane tp;
    private final Button processingButton;
    private final Button completeButton;
    private final Button changeDbButton;
    private ServiceOrder serviceOrder;
    private JsonParser jsonParser;

    public LocalSystemGui(Stage st){
        localService = new LocalServiceImpl();
        dbNameLabel = new Label("Database name:");
        dbNameTxt = new TextField();
        dbUserLabel = new Label("Username:");
        dbUserTxt = new TextField();
        dbPasswordLabel = new Label("Password");
        dbPasswordTxt = new PasswordField();
        dbPane = new GridPane();
        gridPane = new GridPane();
        searchGridPane = new GridPane();
        searchCodeLabel = new Label("Insert your order id:");
        searchCodeTxt = new TextField();
        searchButton = new Button("Search");
        ordersList = FXCollections.observableArrayList();
        connectDbButton = new Button("Test connection");
        resultOrder = new TableView();
        settings = new SettingsServiceIOImpl();
        tp = new TitledPane();
        processingButton = new Button("Process order");
        completeButton = new Button("Dispatch order");
        changeDbButton = new Button("Change DB configuration");

        stage = st;
        stage.setScene(buildScene());
    }

    public Scene buildScene(){

        final ImageView imageView = new ImageView(IMAGE);
        Group imageGroup = new Group(imageView);

        dbPane.setAlignment(Pos.CENTER);
        dbPane.setVgap(10);
        dbPane.setHgap(10);
        dbPane.setPadding(new Insets(15,15,15,15));

        gridPane.setAlignment(Pos.CENTER);
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(15,15,0,15));

        searchGridPane.setAlignment(Pos.CENTER);
        searchGridPane.setVgap(10);
        searchGridPane.setHgap(10);
        searchGridPane.setPadding(new Insets(15,15,0,15));
        searchGridPane.setVisible(false);

        dbPane.add(dbUserLabel,0,0);
        dbPane.add(dbUserTxt,1,0);
        dbPane.add(dbPasswordLabel,0,1);
        dbPane.add(dbPasswordTxt,1,1);
        dbPane.add(dbNameLabel,0,2);
        dbPane.add(dbNameTxt,1,2);
        connectDbButton.setPrefWidth(150);
        dbPane.add(connectDbButton,0,3,2,1);

        searchGridPane.add(searchCodeLabel,0,0);
        searchGridPane.add(searchCodeTxt,1,0);
        searchGridPane.add(searchButton,2,0);
        resultOrder.setEditable(true);
        resultOrder.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        resultOrder.setPrefHeight(150);
        TableColumn orderIdColumn = new TableColumn("Id");
        orderIdColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("orderIdProperty"));
        TableColumn siteIdColumn = new TableColumn("Site");
        siteIdColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("siteIdProperty"));
        TableColumn statusColumn = new TableColumn("Status");
        statusColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("statusProperty"));
        TableColumn pIdColumn = new TableColumn("Parent process Id");
        pIdColumn.setCellValueFactory(new PropertyValueFactory<Order,String>("pIdProperty"));
        resultOrder.setItems(ordersList);
        resultOrder.getColumns().addAll(orderIdColumn,siteIdColumn,statusColumn,pIdColumn);
        searchGridPane.add(resultOrder,0,2,3,1);
        searchGridPane.add(processingButton,0,3);
        searchGridPane.add(completeButton,1,3);
        searchGridPane.add(changeDbButton,2,3);




        if(settings.getDbName()!=null && settings.getDbUsername()!=null && settings.getDbPassword()!=null
                && settings.getDbName().length()>0 && settings.getDbUsername().length()>0 && settings.getDbPassword().length()>0) {
            dbNameTxt.setText(settings.getDbName());
            dbUserTxt.setText(settings.getDbUsername());
            dbPasswordTxt.setText(settings.getDbPassword());
            testDb();
        }


        gridPane.add(imageGroup,0,0);
        tp.setText("DB CONFIGURATION");
        tp.setContent(dbPane);
        gridPane.add(tp,0,1);
        gridPane.add(searchGridPane,0,2);



        Scene scene = new Scene(gridPane,560,560);


        searchButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                if(searchCodeTxt.getText().trim().length()!=0)
                    searchOrder();
            }
        });


        connectDbButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                testDb();
            }
        });


        changeDbButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                changeConfigDb();
            }
        });

        processingButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                Order ord = (Order)resultOrder.getSelectionModel().getSelectedItem();
                if(ord != null){
                    processOrder(ord);
                }
            }
        });

        completeButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                Order ord = (Order)resultOrder.getSelectionModel().getSelectedItem();
                if(ord != null){
                    completeOrder(ord);
                }
            }
        });


        return scene;
    }

    public void completeOrder(Order ord){
        searchGridPane.setDisable(true);
        //add disptach information
        DispatchInfoList dil = new DispatchInfoList();
        DispatchInfoItem item = new DispatchInfoItem();
        ArrayList<DispatchInfoList> dilList = new ArrayList<DispatchInfoList>();
        ArrayList<DispatchInfoItem> itemList = new ArrayList<DispatchInfoItem>();
        itemList.add(item);
        dil.setDispatchInfoItems(itemList);
        dilList.add(dil);
        ord.setDispatchInfoList(dilList);
        getServiceOrder().setDestination("dev.consignment.export.boytrix.queue");
        getServiceOrder().setBrokerUrl("tcp://localhost:61616");
        getServiceOrder().completeOrder(ord);
        searchGridPane.setDisable(false);
    }

    public JsonParser getJsonParserService(){
        if (jsonParser == null){
            jsonParser = new JsonParserGson();
        }
        return jsonParser;
    }

    public ServiceOrder getServiceOrder(){
        if (this.serviceOrder == null){
            serviceOrder = new ServiceOrderImpl(getJsonParserService());
        }
        return serviceOrder;
    }

    private void processOrder(Order ord){
        searchGridPane.setDisable(true);
        getServiceOrder().setDestination("dev.consignment.export.boytrix.queue");
        getServiceOrder().setBrokerUrl("tcp://localhost:61616");
        getServiceOrder().processOrder(ord);
        searchGridPane.setDisable(false);
    }

    private void searchOrder(){
        String orderId = searchCodeTxt.getText().trim();
        if(orderId.length()!=0){
            searchGridPane.setDisable(true);
            List<Order> orders = localService.findOrder(orderId);
            ordersList.removeAll(resultOrder.getItems());
            ordersList.addAll(orders);
            searchGridPane.setDisable(false);
        }else{
            return; //generate exception
        }
    }

    private void changeConfigDb(){
        searchGridPane.setVisible(false);
        dbPane.setDisable(false);
        tp.setExpanded(true);
    }

    private void testDb(){
        settings.setDbUsername(dbUserTxt.getText().trim());
        settings.setDbPassword(dbPasswordTxt.getText().trim());
        settings.setDbName(dbNameTxt.getText().trim());
        if(localService.testConnection()){
            tp.setExpanded(false);
            dbPane.setDisable(true);
            searchGridPane.setVisible(true);
        }else{
            dbUserTxt.setStyle("-fx-control-inner-background: #ffa1a1");
            dbPasswordTxt.setStyle("-fx-control-inner-background: #ffa1a1");
            dbNameTxt.setStyle("-fx-control-inner-background: #ffa1a1");
            tp.setExpanded(true);
            dbPane.setDisable(false);
            searchGridPane.setVisible(false);
        }
    }


}
