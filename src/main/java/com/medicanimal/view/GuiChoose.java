package com.medicanimal.view;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Created by Antonio Coviello on 23/05/16.
 */
public class GuiChoose{

    private final Label labelInfo;
    private final Button paperTrailButton;
    private final Button localSystemButton;
    private final GridPane grid;
    private final Stage stage;

    private static final Image IMAGE = new Image("img/logo.gif");


    public GuiChoose(Stage st) {
        labelInfo = new Label("Where do you want dispatch the order?");
        paperTrailButton = new Button("Papertrail");
        localSystemButton = new Button("Local system");
        grid = new GridPane();
        stage = st;
        st.setScene(buildScene());
    }

    private Scene buildScene(){
        final ImageView imageView = new ImageView(IMAGE);
        Group imageGroup = new Group(imageView);

        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(15,15,0,15));

        grid.add(imageGroup,0,0);
        grid.add(labelInfo,0,1);
        paperTrailButton.setPrefWidth(150);
        grid.add(paperTrailButton,0,2);
        localSystemButton.setPrefWidth(150);
        grid.add(localSystemButton,0,3);

        Scene scene = new Scene(grid,560,230);

        paperTrailButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                PaperTrailGui pgui = new PaperTrailGui(stage);
            }
        });

        localSystemButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent mouseEvent) {
                LocalSystemGui lgui = new LocalSystemGui(stage);
            }
        });

        return scene;
    }

}
