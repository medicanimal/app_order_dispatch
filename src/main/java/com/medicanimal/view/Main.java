package com.medicanimal.view;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Created by antonio on 18/05/16.
 */
public class Main extends Application{

    public static void main(String[] args){
        Main.launch(args);
    }

    public void changeScene(Scene sc){

    }

    public void start(Stage stage) throws Exception {
        stage.setTitle("Dispatch Order");

        GuiChoose gc = new GuiChoose(stage);
        stage.show();
    }
}
