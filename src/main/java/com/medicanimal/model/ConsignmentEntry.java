package com.medicanimal.model;

/**
 * Created by antonio on 16/05/16.
 */
public class ConsignmentEntry {
    private String orderEntryId;
    private String consignmentEntryId;
    private String status;
    private Integer orderQuantity;
    private Integer shipQuantity;
    private Integer allocatedQuantity;
    private String productName;
    private String productId;
    private String stockExpectedDate;
    private Boolean isOutOfStock;
    private Integer consignmentEntryPK;


    /*public ConsignmentEntry(){
        this.orderEntryId = "0";
        this.consignmentEntryId = "0";
        this.status = null;
        this.orderQuantity = Integer.valueOf(1);
        this.shipQuantity = Integer.valueOf(0);
        this.allocatedQuantity = Integer.valueOf(1);
        this.stockExpectedDate = null;
        this.isOutOfStock = false;
        this.consignmentEntryPK = Integer.valueOf(0);
    }*/

    public String getOrderEntryId() {
        return orderEntryId;
    }

    public void setOrderEntryId(String orderEntryId) {
        this.orderEntryId = orderEntryId;
    }

    public String getConsignmentEntryId() {
        return consignmentEntryId;
    }

    public void setConsignmentEntryId(String consignmentEntryId) {
        this.consignmentEntryId = consignmentEntryId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public Integer getShipQuantity() {
        return shipQuantity;
    }

    public void setShipQuantity(Integer shipQuantity) {
        this.shipQuantity = shipQuantity;
    }

    public Integer getAllocatedQuantity() {
        return allocatedQuantity;
    }

    public void setAllocatedQuantity(Integer allocatedQuantity) {
        this.allocatedQuantity = allocatedQuantity;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStockExpectedDate() {
        return stockExpectedDate;
    }

    public void setStockExpectedDate(String stockExpectedDate) {
        this.stockExpectedDate = stockExpectedDate;
    }

    public Boolean getOutOfStock() {
        return isOutOfStock;
    }

    public void setOutOfStock(Boolean outOfStock) {
        isOutOfStock = outOfStock;
    }

    public Integer getConsignmentEntryPK() {
        return consignmentEntryPK;
    }

    public void setConsignmentEntryPK(Integer consignmentEntryPK) {
        this.consignmentEntryPK = consignmentEntryPK;
    }
}
