package com.medicanimal.model;

/**
 * Created by antonio on 16/05/16.
 */
public class DispatchInfoItem {
    private String dispatchId;
    private String orderEntryId;
    private String productId;
    private Integer quantityShipped;

    public DispatchInfoItem(){

    }

    public DispatchInfoItem(String productId){
        this.dispatchId = "1234";
        this.orderEntryId = "0";
        this.productId = productId;
        this.quantityShipped = Integer.valueOf(1);
    }

    public String getDispatchId() {
        return dispatchId;
    }

    public void setDispatchId(String dispatchId) {
        this.dispatchId = dispatchId;
    }

    public String getOrderEntryId() {
        return orderEntryId;
    }

    public void setOrderEntryId(String orderEntryId) {
        this.orderEntryId = orderEntryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantityShipped() {
        return quantityShipped;
    }

    public void setQuantityShipped(Integer quantityShipped) {
        this.quantityShipped = quantityShipped;
    }
}
