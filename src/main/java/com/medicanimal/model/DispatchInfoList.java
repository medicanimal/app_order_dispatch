package com.medicanimal.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by antonio on 16/05/16.
 */
public class DispatchInfoList {
    private String id;
    private String reference;
    private String warehouse;
    private String courier;
    private String trackingNumber;
    private String dispatchDate;
    private List<DispatchInfoItem> dispatchInfoItems;

    public DispatchInfoList(){}

    public DispatchInfoList(String orderId,String productId){
        this.id = "1234";
        this.reference = orderId+"::"+orderId;
        this.warehouse = "MYWAREHOUSE01";
        this.courier = "DPD";
        this.trackingNumber = "ABCDES111";
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -2);
        this.dispatchDate = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format(cal.getTime());
        (dispatchInfoItems = new LinkedList<DispatchInfoItem>()).add(new DispatchInfoItem(productId));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public String getCourier() {
        return courier;
    }

    public void setCourier(String courier) {
        this.courier = courier;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(String dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public List<DispatchInfoItem> getDispatchInfoItems() {
        return dispatchInfoItems;
    }

    public void setDispatchInfoItems(List<DispatchInfoItem> dispatchInfoItems) {
        this.dispatchInfoItems = dispatchInfoItems;
    }
}
