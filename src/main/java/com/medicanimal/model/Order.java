package com.medicanimal.model;

import javafx.beans.property.SimpleStringProperty;

import java.util.List;

/**
 * Created by antonio on 16/05/16.
 */
public class Order {

    private transient SimpleStringProperty orderIdProperty;
    private transient SimpleStringProperty siteIdProperty;
    private transient SimpleStringProperty statusProperty;
    private transient SimpleStringProperty pIdProperty;

    private String orderId;
    private String storeId;
    private String siteId;
    private String command;
    private List<Consignment> consignments;
    private List<DispatchInfoList> dispatchInfoList;
    private String parentProcessId;

    public Order(){
        this.orderIdProperty = new SimpleStringProperty(null);
        this.siteIdProperty = new SimpleStringProperty(null);
        this.statusProperty = new SimpleStringProperty(null);
        this.pIdProperty = new SimpleStringProperty(null);
        this.storeId = "4";
    }

    public Order(String orderId,String siteId,String status,String pId){
        this.orderIdProperty = new SimpleStringProperty(orderId);
        this.siteIdProperty = new SimpleStringProperty(siteId);
        this.statusProperty = new SimpleStringProperty(status);
        this.pIdProperty = new SimpleStringProperty(pId);
    }


    public String getOrderIdProperty() {
        return orderIdProperty.get();
    }

    public SimpleStringProperty orderIdPropertyProperty() {
        return orderIdProperty;
    }

    public void setOrderIdProperty(String orderIdProperty) {
        this.orderIdProperty.set(orderIdProperty);
    }

    public String getSiteIdProperty() {
        return siteIdProperty.get();
    }

    public SimpleStringProperty siteIdPropertyProperty() {
        return siteIdProperty;
    }

    public void setSiteIdProperty(String siteIdProperty) {
        this.siteIdProperty.set(siteIdProperty);
    }

    public String getStatusProperty() {
        return statusProperty.get();
    }

    public SimpleStringProperty statusPropertyProperty() {
        return statusProperty;
    }

    public void setStatusProperty(String statusProperty) {
        this.statusProperty.set(statusProperty);
    }

    public String getpIdProperty() {
        return pIdProperty.get();
    }

    public SimpleStringProperty pIdPropertyProperty() {
        return pIdProperty;
    }

    public void setpIdProperty(String pIdProperty) {
        this.pIdProperty.set(pIdProperty);
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
        this.orderIdProperty.set(orderId);
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
        this.siteIdProperty.set(siteId);
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
        this.statusProperty.set(command);
    }

    public List<Consignment> getConsignments() {
        return consignments;
    }

    public void setConsignments(List<Consignment> consignments) {
        this.consignments = consignments;
    }

    public List<DispatchInfoList> getDispatchInfoList() {
        return dispatchInfoList;
    }

    public void setDispatchInfoList(List<DispatchInfoList> dispatchInfoList) {
        this.dispatchInfoList = dispatchInfoList;
    }

    public String getParentProcessId() {
        return parentProcessId;
    }

    public void setParentProcessId(String parentProcessId) {
        this.parentProcessId = parentProcessId;
        this.pIdProperty.set(parentProcessId);
    }
}
