package com.medicanimal.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by antonio on 16/05/16.
 */
public class Consignment {
    private String consignmentId;
    private String orderId;
    private String storeId;
    private String siteId;
    private String expectedStockArivalDate;
    private Boolean exportedToWarehouse;
    private String warehouseExportedTime;
    private String warehouseId;
    private String command;
    private String status;
    private List<ConsignmentEntry> consignmentEntries;
    private String courier;
    private String trackingNumber;
    private String dispatchDate;
    private Integer consignmentProcessId;
    private String orderProcessId;
    private Integer consignmentPK;

    public Consignment(){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR, -1);
        this.storeId = "4";
        this.siteId = null;
        this.expectedStockArivalDate = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format(cal.getTime());
        cal.add(Calendar.MINUTE, 5);
        this.warehouseExportedTime = (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")).format(cal.getTime());
        this.exportedToWarehouse = true;
        this.warehouseId = "MYWAREHOUSE01";
        this.consignmentEntries = null;
        this.courier = "";
        this.trackingNumber = "";
        this.dispatchDate = null;
        this.consignmentProcessId = Integer.valueOf(0);
        this.orderProcessId = null;
        this.consignmentPK = Integer.valueOf(0);
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getExpectedStockArivalDate() {
        return expectedStockArivalDate;
    }

    public void setExpectedStockArivalDate(String expectedStockArivalDate) {
        this.expectedStockArivalDate = expectedStockArivalDate;
    }

    public Boolean getExportedToWarehouse() {
        return exportedToWarehouse;
    }

    public void setExportedToWarehouse(Boolean exportedToWarehouse) {
        this.exportedToWarehouse = exportedToWarehouse;
    }

    public String getWarehouseExportedTime() {
        return warehouseExportedTime;
    }

    public void setWarehouseExportedTime(String warehouseExportedTime) {
        this.warehouseExportedTime = warehouseExportedTime;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ConsignmentEntry> getConsignmentEntries() {
        return consignmentEntries;
    }

    public void setConsignmentEntries(List<ConsignmentEntry> consignmentEntries) {
        this.consignmentEntries = consignmentEntries;
    }

    public String getCourier() {
        return courier;
    }

    public void setCourier(String courier) {
        this.courier = courier;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getDispatchDate() {
        return dispatchDate;
    }

    public void setDispatchDate(String dispatchDate) {
        this.dispatchDate = dispatchDate;
    }

    public Integer getConsignmentProcessId() {
        return consignmentProcessId;
    }

    public void setConsignmentProcessId(Integer consignmentProcessId) {
        this.consignmentProcessId = consignmentProcessId;
    }

    public String getOrderProcessId() {
        return orderProcessId;
    }

    public void setOrderProcessId(String orderProcessId) {
        this.orderProcessId = orderProcessId;
    }

    public Integer getConsignmentPK() {
        return consignmentPK;
    }

    public void setConsignmentPK(Integer consignmentPK) {
        this.consignmentPK = consignmentPK;
    }
}
